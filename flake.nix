{
  # description = "";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    devshell.url = "github:numtide/devshell";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, devshell, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system: 
      let
        pkgs = import nixpkgs {
          config.allowUnfree = true;
          overlays = [ devshell.overlay ];
          inherit system;
        };
      in {
        devShell = # Used by `nix develop`, value needs to be a derivation
          pkgs.devshell.mkShell {

            commands = [{
              name = "install";
              category = "newsletter";
              help = "Installs the dependencies of the newsletter.";
              command = "pnpm install";
            }{
              name = "dev";
              category = "newsletter";
              help = "Starts the development server.";
              command = "pnpm dev";
            }{
              name = "build";
              category = "newsletter";
              help = "Builds the newsletter.";
              command = "pnpm build";
            }{
              name = "b";
              help = "An alias for `build`.";
              category = "newsletter";
              command = "build";
            }{
              category = "newsletter";
              package = pkgs.nodePackages.pnpm;
            }];

            # Silently installed packages.
            packages = with pkgs; [
              gnused
              nodejs
            ];
          };
      }
  );
}
