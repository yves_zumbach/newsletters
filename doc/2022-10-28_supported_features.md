# Support for Features

- Rounded corners (`border-radius`): 73% full, 18% partial
- Shadows (`box-shadow`): 51% full, 6% partial
- Background image: followed tutorial at https://blog.edmdesigner.com/background-images-in-modern-html-emails/ (2017) to make background images work on as many clients as posisble.

