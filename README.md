# HTML Emails for `zumbach.dev`

## Commands

- `install`: Installs dependencies.
- `dev`: Starts the development server.
- `build`: Builds production-ready HTML emails.

## Documentation

- Maizzle (Tailwind for emails): https://maizzle.com.
- MJML Doc (framework for building emails, good inspiration for code): https://documentation.mjml.io/.
- MJML Live Editor: https://mjml.io/try-it-live/.
- TailwindCSS: https://tailwindcss.com/docs/
- Supported email features: https://www.caniemail.com/
- Supported email features 2: https://www.campaignmonitor.com/css/
- List of email bugs: https://github.com/hteumeuleu/email-bugs/issues
- Tutorials for building resilient web components: https://blog.edmdesigner.com/tag/modern-html-email-tutorial/

## HTML Components for Emails

Building HTML components for emails is a real pain, because there are a lot of very outdated email softwares out there and they all use different rendering engines and allow different subsets of HTML.

Mailjet, a newsletter company has created an open source framework for building emails: MJML.
While I use Maizzle, an overlay for TailwindCSS, it is possible to use the web editor of MJML to generate most of the HTML code to use for components: https://mjml.io/try-it-live/.

## Gmail Email Clipping

Gmail clips emails at approximately 100kB.

